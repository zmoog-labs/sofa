FROM java:openjdk-8u45-jdk
MAINTAINER maurizio.branca@gmail.com
EXPOSE 8080
CMD java -jar sofa-0.0.1-SNAPSHOT.jar
ADD target/sofa-0.0.1-SNAPSHOT.jar
